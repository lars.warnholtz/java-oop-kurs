package _Tests;

import org.junit.jupiter.api.Test;

import _3_Gang_Zimmer_Tuer.*;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TestPolymorphieGangZimmerTuer {
    Tuer tuer = new Tuer();
    Zimmer z1 = new Zimmer();
    Zimmer z2 = new Zimmer();
    Gang g1 = new Gang();
    Gang g2 = new Gang();

    @Test
    public void testTrueZweiUnterschiedlicheZimmer() {
        tuer.setR1(z1);
        tuer.setR2(z2);
        assertTrue(tuer.istTuerOK());
    }

    @Test
    public void testFalseZweiGleicheZimmer() {
        tuer.setR1(z1);
        tuer.setR2(z1);
        assertFalse(tuer.istTuerOK());
    }

    @Test
    public void testTrueZweiUnterschiedlicheGaenge() {
        tuer.setR1(g1);
        tuer.setR2(g2);
        assertTrue(tuer.istTuerOK());
    }

    @Test
    public void testFalseNurEineRaumeinheitGesetzt() {
        tuer.setR1(g1);
        assertFalse(tuer.istTuerOK());
    }

    @Test
    public void testFalseZweiGleicheGaenge() {
        tuer.setR1(g1);
        tuer.setR2(g1);
        assertFalse(tuer.istTuerOK());
    }

    @Test
    public void testTrueGangUndZimmer() {
        tuer.setR1(g1);
        tuer.setR2(z1);
        assertTrue(tuer.istTuerOK());
    }    

    @Test
    public void testGangInstanceOfRauminhalt(){
       assertTrue(g1 instanceof Rauminhalt);
    }

    @Test
    public void testZimmerInstanceOfRauminhalt(){
       assertTrue(z1 instanceof Rauminhalt);
    }
}
