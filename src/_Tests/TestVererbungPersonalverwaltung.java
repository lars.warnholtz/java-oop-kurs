package _Tests;

import org.junit.jupiter.api.Test;

import _1_Personalverwaltung.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TestVererbungPersonalverwaltung {
   Angestellter angestellter = new Angestellter("Schulze");
   Arbeiter arbeiter = new Arbeiter();

   @Test
   public void testAngestellterInstanceOfMitarbeiter(){
      assertTrue(angestellter instanceof Angestellter);
      assertTrue(angestellter instanceof Mitarbeiter);
   }

   @Test
   public void testArbeiterInstanceOfMitarbeiter(){
      assertTrue(arbeiter instanceof Arbeiter);
      assertTrue(arbeiter instanceof Mitarbeiter);
   }

   @Test
   public void testNameAngestellter() {
      assertEquals("Schulze", angestellter.getName());
   
      angestellter.setName("Tina");
      assertEquals("Tina", angestellter.getName());
   }

   @Test
   public void testNameArbeiter()
   {
      assertEquals("unbekannt", arbeiter.getName());
      
      arbeiter.setName("Klaus");
      assertEquals("Klaus", arbeiter.getName());
   }

   // test to check Brutto
   @Test
   public void testBruttoArbeiter() {
      arbeiter.setStdLohn(20);
      arbeiter.setStunden(40);
      assertEquals(800, 0, arbeiter.getBrutto());

      arbeiter.setStdLohn(25);
      assertEquals(1000, 0, arbeiter.getBrutto());

      arbeiter.setStunden(30);
      assertEquals(750, 0, arbeiter.getBrutto());
   }

   @Test
   public void testBruttoAngestellter() {
      angestellter.setBrutto(200);
      assertEquals(200, 0, angestellter.getBrutto());

      angestellter.setBrutto(430);
      assertEquals(430, 0, angestellter.getBrutto());
   }
}