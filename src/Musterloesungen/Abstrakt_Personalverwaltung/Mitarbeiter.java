package Musterloesungen.Abstrakt_Personalverwaltung;

// Klasse ist als abstrakt deklariert 
public abstract class Mitarbeiter
{
    private String name;
    private int persNr;
    private static int autoNr=0;
    
    public Mitarbeiter()
    {
        name="unbekannt";
        autoNr++;
        persNr=autoNr;
    }
   
   public Mitarbeiter(String name)
    {
        this();
        this.name=name;       
    }
    
    //Abstrakte Methode:
    public abstract double getBrutto();
    
    public String toString(){
      return "Name: "+ name+", PersNr.: "+persNr;
    }
    public String getName()
    {
        return name;
    }
    public void setName(String name)
    {
        this.name=name;
    }   
}
