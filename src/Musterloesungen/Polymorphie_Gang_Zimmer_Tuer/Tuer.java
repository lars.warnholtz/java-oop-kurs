package Musterloesungen.Polymorphie_Gang_Zimmer_Tuer;

public class Tuer {
	private Rauminhalt r1=null;
	private Rauminhalt r2=null;

	public Tuer() {
	}

	public void setR1(Rauminhalt r1) {
		this.r1 = r1;
	}

	public void setR2(Rauminhalt r2) {
		this.r2 = r2;
	}

	public boolean istTuerOK() {
		return r1!=r2 && r1 != null && r2 != null;
	}
}
