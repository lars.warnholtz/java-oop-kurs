package _4_Beispiele_Collections.Listen;

import java.util.*;

public class ArrayList_Example {
    public static void main(String args[]) {
        ArrayList<String> list = new ArrayList<String>();// Creating arraylist
        list.add("Mango"); // adding Objects in the List 
        list.add("Apple");  
        list.add("Banana");  
        list.add("Grapes");
        // Traversing list through Iterator
        Iterator<String> itr = list.iterator();

        while (itr.hasNext()) {
            System.out.println(itr.next());
        }

        //Sorting the list
        Collections.sort(list);
        System.out.println(list);

        list.remove(2); //removes from index 2
        list.remove("Pineapple"); //finds and removes "Pineapple"
        list.add(1, "Lemon"); //adds "Lemon" at index 1


        System.out.println("ArrayList: " + list);

        System.out.println("Returning element at index 1: "+list.get(1)); //it will return the 2nd element, because index starts from 0
        list.set(1,"Strawberry");  // changing the Element at Index 1
        System.out.println("Returning the new element at index 1: "+list.get(1)); //it will return the 2nd element, because index starts from 0

        // convert List to Array
        String[] NameArray = list.toArray(new String[list.size()]);
        System.out.println("Printing Array: "+Arrays.toString(NameArray));  
        System.out.println("Printing List: "+list);
        
    }
}
