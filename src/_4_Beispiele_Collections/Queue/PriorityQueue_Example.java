package _4_Beispiele_Collections.Queue;

import java.util.*;

public class PriorityQueue_Example {

    static PriorityQueue<String> queue = new PriorityQueue<String>();

    public static void fillList(){
        queue.add("Amit Sharma");
        queue.add("Vijay Raj");
        queue.add("JaiShankar");
        queue.add("Raj");
    }

    public static void IteratingTheQueue(){
        Iterator<String> itr = queue.iterator();
        while (itr.hasNext()) {
            System.out.println(itr.next());
        }
    }

    public static void main(String args[]) {
        fillList();

        // element() & peek() zeigen den Kopf der Queue an 
        System.out.println("head:" + queue.element()); // Wirft eine Exception wenn die Queue Leer ist
        System.out.println("head:" + queue.peek()); // Gibt null zurück wenn die Queue Leer ist

        IteratingTheQueue();

        // Entfernt das Element am Kopf der Queue
        queue.remove(); // Wirft eine Exception wenn die Queue Leer ist
        queue.poll(); // Gibt null zurück wenn die Queue Leer ist
        System.out.println("after removing two elements:");
        
        IteratingTheQueue();
    }
}