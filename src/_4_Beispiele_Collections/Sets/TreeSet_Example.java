package _4_Beispiele_Collections.Sets;

import java.util.*;

public class TreeSet_Example {
    public static void main(String args[]) {
        // Creating and adding elements
        TreeSet<String> set = new TreeSet<String>();
        set.add("A");
        set.add("B");
        set.add("C");
        set.add("D");
        set.add("E");
        // traversing elements
        Iterator<String> itr = set.iterator();
        while (itr.hasNext()) {
            System.out.println(itr.next());
        }
        System.out.println("Descending Order");
        // traversing descending order
        Iterator<String> itr2 = set.descendingIterator();
        while (itr2.hasNext()) {
            System.out.println(itr2.next());
        }

        // various NavigableSet operations.
        System.out.println("New Set with Integers: ");
        System.out.println("Initial Set: " + set);

        System.out.println("Reverse Set: " + set.descendingSet());

        System.out.println("Head Set: " + set.headSet("C", true));

        System.out.println("SubSet: " + set.subSet("A", false, "E", true));

        System.out.println("TailSet: " + set.tailSet("C", false));

        
        // New TreeSet with integers 
        TreeSet<Integer> set2 = new TreeSet<Integer>();

        set2.add(24);
        set2.add(66);
        set2.add(12);
        set2.add(15);
        
        System.out.println("Highest Value: " + set2.pollFirst());
        System.out.println("Lowest Value: " + set2.pollLast());
    }
}
